import death_causes
import person
import random
import time


class Colony:

    def __init__(self, colony_name, colony_population, colony_couple_dict):

        self.colony_name = colony_name
        self.colony_population = colony_population
        self.colony_couple_dict = colony_couple_dict
        self.army = []
        self.is_in_war = False
        self.enemy = None

    def create_army(self, colony_population, army):
        for person in colony_population:
            if current_year - person.year_of_birth >= 16 and person.gender == "male":
                army.append(person)

    def start_war(self, enemy, army):
        if enemy != army and not enemy.is_in_war and not army.is_in_war:
            # if random.random() > 0.5:
            enemy.enemy = army
            army.enemy = enemy
            enemy.is_in_war = True
            army.is_in_war = True
            print(army.colony_name + ' has declared a war to ' + enemy.colony_name + '.')
        else:
            pass

    def make_one_battle(self, colony1, colony2):
        print(colony1.enemy.colony_name + ' started battle with ' + colony2.enemy.colony_name)
        if len(colony1.army)-1 > len(colony2.army)-1:
            print(colony1.colony_name + ' has won a battle.')
        elif len(colony2.army)-1 > len(colony1.army)-1:
            print(colony2.colony_name + ' has won a battle.')


list_of_colonies = []


def new_colony(colony_name, population_size):
    colony_population = person.create_population(population_size, [])
    new_colony_instance = Colony(colony_name=colony_name, colony_population=colony_population, colony_couple_dict={})
    list_of_colonies.append(new_colony_instance)
    return new_colony_instance


new_colony(colony_name='Mishas colony', population_size=random.randint(a=10, b=100))
new_colony(colony_name='Vovos colony', population_size=random.randint(a=10, b=100))
new_colony(colony_name='Varyas colony', population_size=random.randint(a=10, b=100))


current_year = 2000


def yearly_population_growth(yearly_population, couples_dictionary, year):
    person.get_new_kids(population=yearly_population, couples_dict=couples_dictionary, current_year=year)
    return population


while True:
    # print(list_of_colonies)
    current_year += 1
    print(current_year)
    print(" ")
    for colony in list_of_colonies:
        population_name = colony.colony_name
        print(population_name)
        population = colony.colony_population
        print("len: ", len(population))
        if random.random() >= 0.3:
            enemy_colony = list_of_colonies[random.randint(0, len(list_of_colonies)-1)]
            colony.start_war(enemy_colony, colony)
        if colony.enemy is not None:
            print("enemy: ", colony.enemy.colony_name)
        else:
            print("enemy: ", colony.enemy)
        if colony.is_in_war and colony.enemy is not None:
            print("fdsfadsg")
            colony.make_one_battle(colony, colony.enemy)
        else:
            pass
        couples_dict = colony.colony_couple_dict
        army = colony.army
        colony.create_army(population, army)

        if len(population) == 0:
            print(population_name, "Looses")
            # break
        else:
            pass
        person.infect_part_of_population(population=population, current_year=current_year, couples_dict=couples_dict)
        person.kill_part_of_population(population=population, current_year=current_year, couples_dict=couples_dict)
        # print('unique id kids :', person.list_id)
        person.marry_part_of_population(population=population, percent=0, current_year=current_year,
                                        couples_dict=couples_dict)
        yearly_population_growth(yearly_population=population, couples_dictionary=couples_dict, year=current_year)
        person.divorce_part_of_population(current_year=current_year, couples_dict=couples_dict)
        person.analyze_population(population=population, current_year=current_year)
        print('colony population is ', len(population))
        print('army size is ', len(army))
        print(" ")
        time.sleep(5)
