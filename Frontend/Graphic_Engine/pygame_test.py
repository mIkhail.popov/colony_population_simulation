import pygame
from Frontend.Map_Generation.tile_map import *
from Frontend.Map_Generation.spritesheet import Spritesheet

# basic things
pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("population simulation")

# load tile map

spritesheet = Spritesheet('../Resources/Iimages/erhdtrhtfh.png')

map = TileMap('../Resources/Maps/tile_map_test_Tile Layer 2.csv', spritesheet)
map2 = TileMap('../Resources/Maps/tile_map_test_Tile Layer 1.csv', spritesheet)

# main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            break
    screen.fill((255, 255, 255))
    map2.draw_map(screen)
    map.draw_map(screen)
    pygame.display.update()
