from Backend.Data import death_causes
from Backend.Processes import person_generator
from Backend.Entities import person
import random


def get_person(n):
    personality_types = ['Lawful Good', 'Neutral Good', 'Chaotic Good', 'Lawful Neutral', 'True Neutral',
                         'Chaotic Neutral', 'Lawful Evil', 'Neutral Evil', 'Chaotic Evil']
    age_of_death = random.randint(a=60, b=90)
    year_of_birth = random.randint(1950, 1982)
    vitality = random.randint(1, 100)
    strength = random.randint(1, 100)
    dexterity = random.randint(1, 100)
    intellect = random.randint(1, 100)
    luck = random.randint(1, 100)
    personality_type = random.choice(personality_types)
    full_name_and_gender = person_generator.generate_full_name_and_gender()
    # list_of_causes_of_death = death_causes.list_death()
    # cause_of_death = list_of_causes_of_death[random.randint(0, len(list_of_causes_of_death) - 1)]
    cause_of_death = 'no data'
    if int(full_name_and_gender[2]) == 0:
        gender = "female"
        age_of_death = int(age_of_death * 1.15)
    else:
        gender = "male"
    year_of_death = int(year_of_birth + age_of_death)
    full_name = str(full_name_and_gender[0] + " " + full_name_and_gender[1])
    new_person = person.Person(number=n, name=full_name, age_of_death=age_of_death,
                                 year_of_birth=year_of_birth, year_of_death=year_of_death, gender=gender,
                                 cause_of_death=cause_of_death, is_married=False, partner=None, marriage_year=None,
                                 is_divorced=False, divorce_year=None, orientation=person.pick_orientation())
    return new_person