import random
import csv


class Diseases:

    def __init__(self, name, disease_group, disease_type, death_rate, mortality, get_sick_chance, disability_chance,
                 disabilities, infection_year, life_expectancy, transmission_type, transmission_chance, severity,
                 curability, cure_chance):

        self.name = name
        self.disease_group = disease_group
        self.disease_type = disease_type
        self.death_rate = death_rate
        self.mortality = mortality
        self.get_sick_chance = get_sick_chance
        self.disability_chance = disability_chance
        self.disabilities = disabilities
        self.infection_year = infection_year
        self.life_expectancy = life_expectancy
        self.transmission_type = transmission_type
        self.transmission_chance = transmission_chance
        self.severity = severity
        self.curability = curability
        self.cure_chance = cure_chance


list_diseases = []


def populate_diseases():
    with open('diseases.csv', 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        for row in spamreader:
            # print(random.random())
            diseases = Diseases(name=row[0], disease_group=None, disease_type=None, death_rate=None, mortality=row[3],
                                get_sick_chance=row[2], disability_chance=None, disabilities=[], infection_year=None,
                                life_expectancy=row[5], transmission_type=None, transmission_chance=None, severity=None,
                                curability=row[1], cure_chance=row[4])
            list_diseases.append(diseases)
        return list_diseases


print(populate_diseases())