from Backend.Entities import person
import random
import time

Mishas_colony = person.increase_population(10, [])
Vovos_colony = person.increase_population(10, [])

planet_population_list = [Mishas_colony, Vovos_colony]


def yearly_death(population, current_year):
    person.kill_part_of_population(population=population, current_year=current_year)
    return population


def yearly_population_growth(population_size, population):
    person.increase_population(population_size=population_size, population=population)
    return population


def run(current_year=2000):
    while True:
        current_year += 1
        print(current_year)
        n = 0
        for planet_population in planet_population_list:
            n += 1
            if n % 2 == 0:
                print('Колония ВОВО')
            else:
                print('Колония МИШИ')
            yearly_death(population=planet_population, current_year=current_year)
            yearly_population_growth(population_size=random.randint(0, 150), population=planet_population)
            print('planet_population is ', len(planet_population))
            person.analyze_population(population=planet_population, current_year=current_year)
            person.marry_part_of_population(population=planet_population, percent=0.25, current_year=current_year)
            time.sleep(10)