from Backend.Data import death_causes
import random
import csv

file = open('Backend/Data/gender_refine-csv.csv', 'r')
reader = csv.reader(file)
dict = {}

for row in reader:
    name = row[0]
    gender = row[1]
    dict.update({name: gender})

with open('Backend/Data/last_names.csv', newline='', encoding="utf8") as last_name_file:
    reader = csv.reader(last_name_file)
    last_name_list = list(reader)


def generate_full_name_and_gender():
    random_first_name_and_gender = random.choice(list(dict.items()))
    random_first_name = random_first_name_and_gender[0]
    gender = random_first_name_and_gender[1]
    m = random.randint(a=0, b=98342)
    random_last_name = last_name_list[m][0].capitalize()
    # print('new person: ', random_first_name, random_last_name, gender)
    return random_first_name, random_last_name, gender
