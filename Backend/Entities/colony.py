import person


class Colony:
    def __init__(self, id, name, population, army, economy, status, statistics):
        self.id = id
        self.name = name
        self.population = population
        self.army = army
        self.economy = economy
        self.status = status
        self.statistics = statistics
