class Event:

    def __init__(self, id, name, colony, active, time, context, parent_event, child_event, title, text, options, limit):
        self.id = id
        self.name = name
        self.colony = colony
        self.active = active
        self.time = time
        self.context = context
        self.parent_event = parent_event
        self.child_event = child_event
        self.title = title
        self.text = text
        self.options = options
        self.limit = limit

    def start(self):
        self.active = True

    def end(self):
        self.active = False
