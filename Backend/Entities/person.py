from Backend.Data import death_causes
from Backend.Functions import death_predictor
from random import *
import random
import string

list_id = []
new_couples_count = 0


class Person:

    def __init__(self, number, name, age_of_death, year_of_birth, year_of_death, gender, cause_of_death, is_married,
                 partner, marriage_year, is_divorced, divorce_year, orientation):

        self.number = number
        self.name = name
        self.age_of_death = age_of_death
        self.year_of_birth = year_of_birth
        self.year_of_death = year_of_death
        self.gender = gender
        self.cause_of_death = cause_of_death
        self.is_married = is_married
        self.partner = partner
        self.marriage_year = marriage_year
        self.is_divorced = is_divorced
        self.divorce_year = divorce_year
        self.orientation = orientation
        # self.kids = kids
        # self.vitality = vitality
        # self.strength = strength
        # self.dexterity = dexterity
        # self.intellect = intellect
        # self.luck = luck
        # self.personality_type = personality_type
        # self.diseases = diseases
        # self.child_id = child_id

    def marry(self, person, marriage_year):
        self.is_married = True
        self.partner = person
        self.marriage_year = marriage_year

    def divorce(self, divorce_year):
        self.is_divorced = True
        self.partner = None
        self.divorce_year = divorce_year
        self.is_married = False

    def get_disease(self, diseases, current_year):
        self.diseases.update({diseases: current_year})

    def cure_disease(self, diseases):
        self.diseases.pop(diseases)


def pick_orientation():
    chance_roll = random.random()
    if chance_roll < 0.827:
        return 'heterosexual'
    elif 0.827 <= chance_roll < 0.931:
        return 'bisexual'
    else:
        return 'homosexual'


def generate_unique_id(child_id):
    child_id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    if not child_id == list_id:
        list_id.append(child_id)
    return child_id


def create_population(population_size, population):
    male_count = 0
    for n in range(population_size):
        person_info = death_predictor.get_person()
        population.append(Person(number=n, name=person_info.get('full_name'),
                                 age_of_death=person_info.get('age_of_death'),
                                 year_of_birth=person_info.get('year_of_birth'),
                                 year_of_death=person_info.get('year_of_death'), gender=person_info.get('gender'),
                                 cause_of_death=person_info.get('cause_of_death'),
                                 is_married=False, partner=None, marriage_year=None,
                                 is_divorced=False, divorce_year=None, orientation=pick_orientation(), kids=[],
                                 vitality=person_info.get('vitality'), strength=person_info.get('strength'),
                                 dexterity=person_info.get('dexterity'), intellect=person_info.get('intellect'),
                                 luck=person_info.get('luck'), personality_type=person_info.get('personality_type'),
                                 diseases=None, child_id=None))

    return population


def analyze_population(population, current_year):
    count_man = 0
    count_women = 0
    count_kids = 0

    for person in population:
        if person.gender == "female":
            count_women += 1
        elif person.gender == "male":
            count_man += 1
        elif current_year - person.year_of_birth < 16:
            count_kids += 1
        else:
            print(person.cause_of_death)

    # print("male: ", count_man, count_man / len(population))
    # print("female: ", count_women, count_women / len(population))
    # print("kids: ", count_kids, count_kids / len(population))


def take_partners_last_name(person_1, person_2):
    person_1_last_name = person_1.name[person_1.name.find(' ') + 1:]
    person_2_last_name = person_2.name[person_2.name.find(' ') + 1:]
    if person_1.gender == 'female' and person_2.gender == 'male':
        person_1.name = person_1.name[:person_1.name.find(' ') + 1] + person_2_last_name
    elif person_2.gender == 'female' and person_1.gender == 'male':
        person_2.name = person_2.name[:person_2.name.find(' ') + 1] + person_1_last_name
    else:
        pass


def take_parent_last_name(child, parent):
    person_2_last_name = parent.name[parent.name.find(' ') + 1:]
    child.name = child.name[:child.name.find(' ') + 1] + person_2_last_name


def get_new_kids(population, couples_dict, current_year):
    count_new_kids = 0
    for key in couples_dict:
        roll = random.random()
        if roll < 0.9674:
            number_of_kids = 1
        elif 0.9674 >= roll < 0.99910277:
            number_of_kids = 2
        else:
            number_of_kids = 3
        count_new_kids += number_of_kids
        for n in range(number_of_kids):
            person_info = death_predictor.get_person()
            child = Person(number=n, name=person_info.get('full_name'),
                           age_of_death=person_info.get('age_of_death'), year_of_birth=current_year,
                           year_of_death=current_year + person_info.get('year_of_death'),
                           gender=person_info.get('gender'), cause_of_death=person_info.get('cause_of_death'),
                           is_married=False, partner=None, marriage_year=None, is_divorced=False, divorce_year=None,
                           orientation=pick_orientation(), kids=[], vitality=person_info.get('vitality'),
                           dexterity=person_info.get('dexterity'), intellect=person_info.get('intellect'),
                           strength=person_info.get('strenght'), luck=person_info.get('luck'),
                           personality_type=person_info.get('personatily_type'),
                           diseases={},
                           child_id=generate_unique_id(
                               child_id=''.join(random.choices(string.ascii_uppercase + string.digits, k=10))))
            take_parent_last_name(child=child, parent=key)
            population.append(child)
            key.kids.append(child)
            key.partner.kids.append(child)
            # print('Name of kids: ', child.name)
            if key.diseases is not None and len(key.diseases) != 0 and key.partner.diseases is not None:
                key.partner.diseases.update({key.disease: current_year})
                print(key.partner.name, " got sick form ", key.name)
    print(count_new_kids, " kids were born")


def create_a_couple(person_1, person_2, current_year):
    person_1.marry(person_2, current_year)
    person_2.marry(person_1, current_year)
    take_partners_last_name(person_1, person_2)

    # print(person_1.gender, person_1.name, 'has formed a couple with', person_2.gender, person_2.name)


def divorce_couple(person_1, person_2, current_year, couples_dict):
    person_1.divorce(divorce_year=current_year)
    person_2.divorce(divorce_year=current_year)
    if person_1 in couples_dict:
        del couples_dict[person_1]
    elif person_2 in couples_dict:
        del couples_dict[person_2]
    else:
        pass


def if_person_is_married(couples_dict, current_year, person):
    if person.is_married:
        print("he left behind a widow ", person.partner.name, " and ", len(person.kids), " kids")
        divorce_couple(person_1=person, person_2=person.partner, current_year=current_year,
                              couples_dict=couples_dict)


def get_random_diseases():
    random_disease = random.randint(a=0, b=len(death_causes.list_diseases) - 1)
    return death_causes.list_diseases[random_disease]


def infect_part_of_population(population, current_year, couples_dict):
    total_infected = 0
    total_dead_from_sick = 0
    total_cured = 0
    person_info = death_predictor.get_person()
    per = Person(number=None, name=None, age_of_death=person_info.get('age_of_death'), year_of_birth=None,
                 year_of_death=person_info.get('year_of_death'), gender=None,
                 cause_of_death=None, is_married=None,
                 partner=None, marriage_year=None, is_divorced=None, divorce_year=None, orientation=None, kids=None,
                 vitality=None, strength=None, dexterity=None,
                 intellect=None, luck=None, personality_type=None, diseases={}, child_id=None)

    for person in population:
        disease = get_random_diseases()
        if float(disease.get_sick_chance) <= random.random():
            per.get_disease(diseases=disease, current_year=current_year)
            if not disease.life_expectancy == '0':
                per.age_of_death = per.age_of_death - int(disease.life_expectancy)
            # print(per.get_disease(diseases=disease.name, current_year=infection_year)
            # print(person.name, " got sick with ", disease.name)
            total_infected += 1
            if disease in per.diseases:
                if float(disease.cure_chance) >= random.random() and disease.curability == 'True':
                    # print(person.name, 'cured from', disease.name)
                    per.cure_disease(diseases=disease)
                    total_cured += 1
                    print("Total cured: ", total_cured)
                    break
                elif float(disease.mortality) <= random.random():
                    # print(person.name, " has died sick with ", disease.name)
                    total_dead_from_sick += 1
                    if_person_is_married(couples_dict, current_year, person)
                    population.remove(person)
    print("Total infected: ", total_infected)
    print("Total dead from sick: ", total_dead_from_sick)

'''make couples dict a global variable or smth'''
def kill_part_of_population(population, current_year, couples_dict={}):
    kill_count = 0
    for person in population:
        if person.year_of_death <= current_year:
            kill_count += 1
            print(person.name, " has died at the age of ", person.age_of_death, " cause of death ",
                  person.cause_of_death)
            if person.is_married:
                print("he left behind a widow ", person.partner.name, " and ", len(person.kids), " kids")
                divorce_couple(person_1=person, person_2=person.partner, current_year=current_year,
                               couples_dict=couples_dict)
            population.remove(person)
    print('Total Dead: ', kill_count)


def increase_population(population_size, population):
    for n in range(population_size):
        new_person = death_predictor.get_person(n)
        population.append(new_person)
    return population


def marry_part_of_population(population, percent, current_year, couples_dict):
    global new_couples_count
    new_couples_count = 0

    def roll_check_marry(partner_1, partner_2, year, couples_dictionary, child_id):
        roll = random.random()
        if roll >= percent:
            create_a_couple(person_1=partner_1, person_2=partner_2, current_year=year)
            couples_dictionary[person] = other_person
            global new_couples_count
            new_couples_count += 1

    computation_count = 0
    for person in population:
        roll_range = random.randint(1, (int(len(population))))
        if roll_range < 1:
            roll_range = 1
        for n in range(0, roll_range):
            other_person = choice(population)
            if current_year - person.year_of_birth < 16:
                break
            else:
                pass
            # if person.child_id == None and other_person.child_id == None:
            #     pass
            # else:
            #     if not person.child_id == other_person.child_id:
            #         pass
            #     else:
            #         break
            if (person.is_divorced and person.divorce_year >= current_year - 2) or not person.is_divorced:
                if not person == other_person and not other_person.is_married and not person.is_married and \
                        (not other_person.is_divorced or other_person.divorce_year >= current_year - 2):
                    if person.orientation == 'heterosexual' and (other_person.orientation == 'heterosexual' or
                                                                 other_person.orientation == 'bisexual') and not \
                            person.gender == other_person.gender:
                        roll_check_marry(person, other_person, current_year, couples_dict,
                                         child_id=''.join(random.choices(string.ascii_uppercase + string.digits)))
                    elif person.orientation == 'homosexual' == other_person.orientation \
                            and person.gender == other_person.gender:
                        roll_check_marry(person, other_person, current_year, couples_dict,
                                         child_id=''.join(random.choices(string.ascii_uppercase + string.digits)))
                    elif person.orientation == 'bisexual' == other_person.orientation:
                        roll_check_marry(person, other_person, current_year, couples_dict,
                                         child_id=''.join(random.choices(string.ascii_uppercase + string.digits)))
                    elif person.orientation == 'bisexual' and other_person.orientation == 'heterosexual' and not \
                            person.gender == other_person.gender:
                        roll_check_marry(person, other_person, current_year, couples_dict,
                                         child_id=''.join(random.choices(string.ascii_uppercase + string.digits)))
                    elif person.orientation == 'bisexual' and other_person.orientation == 'homosexual' and \
                            person.gender == other_person.gender:
                        roll_check_marry(person, other_person, current_year, couples_dict,
                                         child_id=''.join(random.choices(string.ascii_uppercase + string.digits)))
                        computation_count += 1
                    else:
                        pass
                else:
                    pass
            else:
                break
    print(new_couples_count, 'new couples')
    return couples_dict


def divorce_part_of_population(couples_dict, current_year):
    divorce_count = 0
    roll_divorce = random.random()
    for key in list(couples_dict):
        if roll_divorce > 0.3666:
            pass
        else:
            divorce_count += 1
            # print(key.name, " divorce with ", key.partner.name)
            divorce_couple(person_1=key, person_2=key.partner, current_year=current_year, couples_dict=couples_dict)
    print(divorce_count, " divorces")
